/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package No1;

import java.util.ArrayList;

/**
 *
 * @author ACER
 */
public class Nilai {
    ArrayList<Mahasiswa> tampung;
    
    public Nilai(){
        tampung = new ArrayList<Mahasiswa>();
    }
    public void isiData(String nama, String nim, double nilai){
        tampung.add(new Mahasiswa(nama,nim,nilai));
    }
    public void showData() {
        for(Mahasiswa nilai : tampung){
            System.out.println("Nama : "+ nilai.getName()+
                    "" + ", NIM : " + nilai.getNim() + 
                    "" + ", Nilai : " + nilai.getNilai());
            
        }
    }
    public void showRata2(){
        double jumlah = 0;
       
            for(int i = 0; i < tampung.size();i++){
                jumlah = jumlah + tampung.get(i).getNilai();
            }
            double rata2;
            rata2 = jumlah / tampung.size();
            System.out.println("Rata - rata : "+rata2);
        
    }
}
