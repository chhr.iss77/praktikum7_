/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package No2;

/**
 *
 * @author ACER
 */
public class Pegawai {
    private String NIP;
    private String Nama;
    private double GajiPerhari;

    public Pegawai(String NIP, String Nama, double GajiPerhari) {
        this.NIP = NIP;
        this.Nama = Nama;
        this.GajiPerhari = GajiPerhari;
    }

    public String getNIP() {
        return NIP;
    }

    public String getNama() {
        return Nama;
    }

    public double getGajiPerhari() {
        return GajiPerhari;
    }

    public void setNIP(String NIP) {
        this.NIP = NIP;
    }

    public void setNama(String Nama) {
        this.Nama = Nama;
    }

    public void setGajiPerhari(double GajiPerhari) {
        this.GajiPerhari = GajiPerhari;
    }
    
    
}
