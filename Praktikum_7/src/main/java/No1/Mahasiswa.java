/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package No1;

/**
 *
 * @author ACER
 */
public class Mahasiswa {
    String name;
    String nim;
    double nilai;

    public Mahasiswa(String name, String nim,double nilai) {
        this.name = name;
        this.nim = nim;
        this.nilai = nilai;
    }

    public String getName() {
        return name;
    }

    public String getNim() {
        return nim;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setNim(String nim) {
        this.nim = nim;
    }

    public double getNilai() {
        return nilai;
    }

    public void setNilai(double nilai) {
        this.nilai = nilai;
    }
    
    
    
}
