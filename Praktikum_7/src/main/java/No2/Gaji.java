/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package No2;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;

/**
 *
 * @author ACER
 */
public class Gaji {
    ArrayList<Pegawai> pegawai;

    public Gaji() {
        pegawai = new ArrayList<Pegawai>();
    }
    public void inputPegawai(String NIP, String Nama, double GajiPerhari){
        pegawai.add(new Pegawai(NIP,Nama,GajiPerhari));
    }
    public void ShowData(){
        for(Pegawai gaji : pegawai ){
            System.out.println("Nama : "+gaji.getNama());
            System.out.println("NIP : "+gaji.getNIP());
            System.out.println("Gaji : "+gaji.getGajiPerhari());
        }
    }
    public static int numberOfDaysInMonth(int month, int year){
        Calendar monthStart = new GregorianCalendar(year,month,1);
        return monthStart.getActualMaximum(Calendar.DAY_OF_MONTH);
    }
    public void gaji(String bln){
        double GajiPerBulan;
        String bul = bln.toLowerCase();
        int bulan = 0;
        switch (bul){
            case "januari" :
                bulan = 0;
                break;
            case "februari" :
                bulan = 1;
                break;
            case "maret" :
                bulan = 2;
                break;
            case "april" :
                bulan = 3;
                break;
            case "mei" :
                bulan = 4;
                break;
            case "juni" :
                bulan = 5;
                break;
            case "juli" :
                bulan = 6;
                break;   
            case "agustus" :
                bulan = 7;
                break;
            case "september" :
                bulan = 8;
                break;
            case "oktober" :
                bulan = 9;
                break;    
        }
        for(Pegawai gaji : pegawai){
            int hari = numberOfDaysInMonth(bulan,2017);
            double mk = hari * gaji.getGajiPerhari();
            gaji.setGajiPerhari(mk);
        }
    }
    
    
}
