
package No2;

import java.util.ArrayList;
import java.util.Scanner;

/**
 *
 * @author ACER
 */
public class Main {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        ArrayList<Pegawai> data = new ArrayList<Pegawai>();
        
        Gaji gaji = new Gaji();

        System.out.println("masukan banyak pegawai:");
        int x = sc.nextInt();
        String nip;

        for (int i = 0; i < x; i++) {
            while (true) {
                System.out.println("Masukan Nip :");
                nip = sc.next();
                if (nip.length() == 9) {
                    break;
                } else {
                    System.out.println("input harus angka dan berjumlah 9");
                }
            }
            System.out.println("Masukan Nama :");
            String nm = sc.next();
            System.out.println("masukan gaji :");
            double gj = sc.nextDouble();
            gaji.inputPegawai(nip, nm, gj);
            System.out.println("");
        }
        System.out.println("Masukan Bulan (ex: januari) : ");
        String bln = sc.next();
        System.out.println("");
        gaji.gaji(bln);
        gaji.ShowData();
    }
}
